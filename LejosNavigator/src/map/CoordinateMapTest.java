package map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CoordinateMapTest {

	private CoordinateMap map;

	@Before
	public void setUp() throws Exception {
		map = new CoordinateMap();
	}

	@After
	public void tearDown() throws Exception {
		map = null;
	}

	@Test
	public void testMoveOnceRightAngle() {

		map.move(90, 100);

		assertEquals(90, map.getAngle(), 0.1);
		assertEquals(100, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testMoveTwiceRightAngle() {

		map.move(90, 100);
		map.move(90, 100);

		assertEquals(180, map.getAngle(), 0.1);
		assertEquals(100, map.getHorizontalPosition(), 0.1);
		assertEquals(-100, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testMoveOnceNotRightAngle() {

		map.move(45, 100);

		assertEquals(45, map.getAngle(), 0.1);
		assertEquals(70.7, map.getHorizontalPosition(), 0.1);
		assertEquals(70.7, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testMoveTwiceNotRightAngle() {

		map.move(45, 100);
		map.move(45, 100);

		assertEquals(90, map.getAngle(), 0.1);
		assertEquals(170.7, map.getHorizontalPosition(), 0.1);
		assertEquals(70.7, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testMoveOnceHighAngle() {

		map.move(288, 100);

		assertEquals(288, map.getAngle(), 0.1);
		assertEquals(-95.1, map.getHorizontalPosition(), 0.1);
		assertEquals(30.9, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testMoveTwiceOverflowAngle() {

		map.move(288, 100);
		map.move(200, 200);

		assertEquals(128, map.getAngle(), 0.1);
		assertEquals(62.5, map.getHorizontalPosition(), 0.1);
		assertEquals(-92.2, map.getVertikalPosition(), 0.1);

	}

	// -------- TO START TEST CASES -----------

	@Test
	public void testToStartMoveOnceRightAngle() {

		map.move(90, 100);
		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(180, arch, 0.1);
		assertEquals(100, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testToStartMoveTwiceRightAngle() {

		map.move(90, 100);
		map.move(90, 100);

		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(135, arch, 0.1);
		assertEquals(141.4, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testToStartMoveOnceNotRightAngle() {

		map.move(45, 100);

		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(180, arch, 0.1);
		assertEquals(100, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testToStartMoveTwiceNotRightAngle() {

		map.move(45, 100);
		map.move(45, 100);

		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(157.5, arch, 0.1);
		assertEquals(184.8, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testToStartMoveOnceHighAngle() {

		map.move(288, 100);

		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(180, arch, 0.1);
		assertEquals(100, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

	@Test
	public void testToStartMoveTwiceOverflowAngle() {

		map.move(288, 100);
		map.move(200, 200);

		double[] archDist = map.getAngleAndDistanceToStart();
		double arch = archDist[0];
		double dist = archDist[1];

		assertEquals(197.9, arch, 0.1);
		assertEquals(111.4, dist, 0.1);

		assertEquals(0, map.getHorizontalPosition(), 0.1);
		assertEquals(0, map.getVertikalPosition(), 0.1);

	}

}
