package map;

/**
 * Coordination map for roboter orientation
 * 
 * @author Vitus Pesch
 *
 */
public class CoordinateMap {

	/**
	 * Angle coordinate in clock quarters visualization
	 * 
	 * @author Vitus Pesch
	 *
	 */
	private static enum AngleClockQuarter {
		FIRST {
			@Override
			public double getNormalizedAngle(double angle) {
				return angle;
			}
		},
		SECOND {
			@Override
			public double getNormalizedAngle(double angle) {
				return angle - 90;
			}
		},
		THIRD {
			@Override
			public double getNormalizedAngle(double angle) {
				return angle - 180;
			}
		},
		FOURTH {
			@Override
			public double getNormalizedAngle(double angle) {
				return angle - 270;
			}
		};

		/**
		 * @param angle
		 * @return arch<90 for trigonometric calculation
		 */
		public abstract double getNormalizedAngle(double angle);

		/**
		 * @param angle
		 * @return clock quarter of map
		 */
		private static AngleClockQuarter getQuarter(double angle) {
			if (angle < 90) {
				return FIRST;
			} else if (angle < 180) {
				return SECOND;
			} else if (angle < 270) {
				return THIRD;
			} else {
				return FOURTH;
			}
		}
	}

	double horiPos = 0;
	double vertPos = 0;
	double angle = 0;

	public double[] move(double angleToTurn, double distance) {
		while (angleToTurn >= 360) {
			angleToTurn -= 360;
		}

		this.angle += angleToTurn;
		if (this.angle >= 360) {
			this.angle -= 360;
		}

		AngleClockQuarter directionQuarter = AngleClockQuarter
				.getQuarter(angle);

		double normAngle = directionQuarter.getNormalizedAngle(angle);
		double hypothenus = distance;
		double oppLeg = Math.sin(Math.toRadians(normAngle)) * distance;
		double leg = Math.sqrt(Math.pow(hypothenus, 2) - Math.pow(oppLeg, 2));

		switch (directionQuarter) {
		case FIRST:
			horiPos += oppLeg;
			vertPos += leg;
			break;
		case SECOND:
			horiPos += leg;
			vertPos -= oppLeg;
			break;
		case THIRD:
			horiPos -= oppLeg;
			vertPos -= leg;
			break;
		case FOURTH:
			horiPos -= leg;
			vertPos += oppLeg;
			break;
		default:
			throw new AssertionError();
		}

		return getCurrentPosition();

	}

	/**
	 * Reduces angle by 90 as long while its over 90
	 * 
	 * @param angle
	 * @return angle < 90
	 */
	public double getNormalizedAngle(double angle) {
		while (angle > 90) {
			angle -= 90;
		}
		return angle;
	}

	public double[] getAngleAndDistanceToStart() {
		return getAngleAndDistanceToPosition(0, 0);
	}

	public double[] getAngleAndDistanceToPosition(double horizontalPosition,
			double verticalPosition) {

		double hypothenuse = Math.sqrt(Math.pow(horiPos - horizontalPosition, 2)
				+ Math.pow(vertPos - verticalPosition, 2));

		double oppLeg;
		double angleAdder;

		if (horiPos >= 0) {
			if (vertPos >= 0) {
				angleAdder = 180;
				oppLeg = horiPos;
			} else {
				angleAdder = 270;
				oppLeg = vertPos;
			}
		} else {
			if (vertPos >= 0) {
				angleAdder = 90;
				oppLeg = vertPos;
			} else {
				angleAdder = 0;
				oppLeg = horiPos;
			}
		}

		double endAngle = Math
				.toDegrees(Math.asin(Math.abs(oppLeg) / Math.abs(hypothenuse)))
				+ angleAdder;

		if (endAngle < angle) {
			endAngle += 360;
		}

		double angleToMove = endAngle - angle;

		move(angleToMove, hypothenuse);

		return new double[] { angleToMove, hypothenuse };
	}

	public double getHorizontalPosition() {
		return horiPos;
	}

	public double getVertikalPosition() {
		return vertPos;
	}

	public double[] getCurrentPosition() {
		return new double[] { horiPos, vertPos };
	}

	public double getAngle() {
		return angle;
	}

}
